import IndexOne from "./IndexOne";
import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";

function App() {
  return (
    <BrowserRouter>
    <Routes>
    <Route path ="/" element={<IndexOne/>}/>
    </Routes>
          </BrowserRouter>
  );
}

export default App;
