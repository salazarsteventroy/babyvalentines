import React, { useState, useEffect } from "react";

const IndexOne = () => {
  const catAngry = "/contents/catAngry.gif";
  const catHappy = '/contents/cathappy.gif';

  const heart = '/contents/heart.gif'

  const [imageSrc, setImageSrc] = useState("/contents/catbeg.gif");
  const [buttonStyle, setButtonStyle] = useState({
    backgroundColor: "#E6A4B4",
    border: "3px solid #FFF8E3",
    fontFamily: "Pixel",
    width: "100%",
    height: "50px",
    fontSize: "25px",
    cursor: "pointer",
    width: "100px",
    color:"#0F0F0F"
  });

  const [buttonStyle2, setButtonStyle2] = useState({
    backgroundColor: "#E6A4B4",
    border: "3px solid #FFF8E3",
    fontFamily: "Pixel",
    width: "100%",
    height: "50px",
    fontSize: "25px",
    cursor: "pointer",
    width: "100px",
    color:"#0F0F0F"
  });

  const [maxX, setMaxX] = useState(window.innerWidth - 200);
  const [maxY, setMaxY] = useState(window.innerHeight - 100);
  const [clickCount, setClickCount] = useState(0);
  const [changeBackground, setChangeBackground] = useState(false);
  const [changeText, setChangeText] = useState(false);
  const [yesClicked, setYesClicked] = useState(false); // State to track if Yes button is clicked
  const [heartPositions, setHeartPositions] = useState([]);

  useEffect(() => {
    const handleResize = () => {
      setMaxX(window.innerWidth - 200);
      setMaxY(window.innerHeight - 100);
    };
    window.addEventListener("resize", handleResize);
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  useEffect(() => {
    if (clickCount >= 5) {
      setImageSrc(catAngry);
      setChangeBackground(true);
      setChangeText(true);
    }
  }, [clickCount, catAngry]);

  const divStyle = {
    backgroundImage: 'url("/contents/WiC5.gif")',
    backgroundRepeat: "repeat-y",
    backgroundSize: "cover",
    height: "100vh",
    width: "100%",
    backgroundColor: "#F9F5F6",
    fontFamily: "Pixel, sans-serif",
    textAlign: "center",
    paddingTop: "20vh",
    fontSize: "70px",
    color: "#E493B3",
    alignItems: "center",
    display: "flex",
    flexDirection: "column",
  };

  const divStyle2 = {
    backgroundImage: `url(${imageSrc})`,
    backgroundRepeat: "no-repeat",
    backgroundSize: "contain",
    height: "500px",
    width: "100%",
    backgroundPositionX: "center",
    backgroundPositionY: "center",
  };

  const handleYesButtonClick = () => {
    setImageSrc(catHappy);
    setYesClicked(true);
    const newHeartPositions = [];
    for (let i = 0; i < 15; i++) {
      const randomX = Math.random() * maxX;
      const randomY = Math.random() * maxY;
      newHeartPositions.push({ x: randomX, y: randomY });
    }
    setHeartPositions(newHeartPositions);
  };


  const handleNoButtonClick = () => {
    // If the "Yes" button has been clicked, keep the image source as cathappy.gif
    if (yesClicked) {
      setImageSrc(catHappy);
      return; // Exit the function early to prevent further execution
    }
  
    // Increment click count
    const updatedClickCount = clickCount + 1;
    setClickCount(updatedClickCount);
  
    // Check if the number of clicks has reached 5
    if (updatedClickCount >= 5) {
      // Change background image to catAngry
      setImageSrc(catAngry);
      // Change text to "You WILL be my valentine"
      setChangeText(true);
    } else {
      // Change the image source to cat frustrated
      setImageSrc("/contents/catfrustrated.gif");
      // Set timeout to revert the image back to original after 3 seconds
      setTimeout(() => {
        setImageSrc("/contents/catbeg.gif");
      }, 3000);
    }
  
    // Generate random position for the button
    const buttonWidth = 100; // Width of the button
    const buttonHeight = 50; // Height of the button
    const maxX = window.innerWidth - buttonWidth; // Maximum X position
    const maxY = window.innerHeight - buttonHeight; // Maximum Y position
    const randomX = Math.random() * maxX;
    const randomY = Math.random() * maxY;
    // Limit the button position to stay within the screen
    const limitedX = Math.min(Math.max(randomX, 0), maxX);
    const limitedY = Math.min(Math.max(randomY, 0), maxY);
    setButtonStyle({
      ...buttonStyle,
      position: "absolute",
      top: limitedY + "px",
      left: limitedX + "px",
    });
  };




  return (
    <div style={divStyle}>
      {heartPositions.map((pos, index) => (
        <img
          key={index}
          src={heart}
          alt="heart"
          style={{
            position: "absolute",
            top: pos.y + "px",
            left: pos.x + "px",
            width: "50px",
            height: "auto",
          }}
        />
      ))}
      <span>
        {yesClicked ? "Yayyy I love you!!" : changeText ? "You WILL be my valentine" : "Will you be my valentine? "}
      </span>
      <span
        style={{
          fontSize:"20px", width:"100%", marginLeft:"300px", marginRight:"300px"
        }}
      >
        we'll eat at home, kiss, cuddle, (not)make a baby, buy overpriced coffee, get you flowers,  watch something that we won't be able to finish , <br/> kiss again, hug you tightly, kiss again, buko juice me, buy snacks, eat ice cream, drive somewhere whatever
      </span>
      <div style={divStyle2}></div>
      <div className="buttons" style={{ display: "flex", justifyContent: "center", gap: "10px", alignItems: "center", position: "relative" }}>
        {!yesClicked && (
          <button
            style={buttonStyle2}
            onClick={handleYesButtonClick}
          >
            Yes
          </button>
        )}
      </div>
      {!yesClicked && (
        <button
          style={buttonStyle}
          onClick={handleNoButtonClick}
        >
          No
        </button>
      )}
    </div>
  );
      }  


export default IndexOne;
